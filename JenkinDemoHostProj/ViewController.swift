//
//  ViewController.swift
//  JenkinDemoHostProj
//
//  Created by Ashwini on 10/01/18.
//  Copyright © 2018 Ashwini. All rights reserved.
//

import UIKit
import Foundation

class ViewController: UIViewController {

    @IBOutlet weak var hellolabel: UILabel!
    var inputArray : Array = ["First random text","This is second here", "I am not sad even if i am Third", "I am glad i am not last but fourth", "I am Fifth and last"];
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func btnChangePressed(_ sender: Any) {
        let index = Int(arc4random_uniform(5));
        hellolabel.text = inputArray[index];
    }
}

